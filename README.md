# ERC20 Solidity Token
    
 - Contract Address: 0x9253DC2A6284d64b3BE59F3e2b570A29cf4db565
 - Symbol: THE_COIN

### Local build

 - ```git clone git@bitbucket.org:lbernardomaia/erc20-solidity-token.git```
 - ```cd erc20-solidity-token```
 - ```docker build -t erc20-solidity-token .```

#### To run

- For local: 
   ```
   docker run --rm -d \
   -e OWNER_ADDRESS=OWNER_ADDRESS \
   -e OWNER_PRIVATE_KEY=OWNER_PRIVATE_KEY \
   -e INFURE_URL=https://ropsten.infura.io/v3/ \
   -e INFURA_KEY=INFURA_KEY \
   -e CONTRACT_ADDRESS=CONTRACT_ADDRESS \
   -p 3000:3000 \
   --rm --name erc20-solidity-token
   ```

-  from DockerHub:
   ```
   docker run --rm -d \
   -e OWNER_ADDRESS=OWNER_ADDRESS \
   -e OWNER_PRIVATE_KEY=OWNER_PRIVATE_KEY \
   -e INFURE_URL=https://ropsten.infura.io/v3/ \
   -e INFURA_KEY=INFURA_KEY \
   -e CONTRACT_ADDRESS=CONTRACT_ADDRESS \
   -p 3000:3000 \
   --rm --name erc20-solidity-token lbernardomaia/erc20-solidity-token
   ```


### CURL commands

To get contract Symbol: ```curl http://localhost:3000/symbol```

To get total supply: ```curl http://localhost:3000/supply```

To distribute: ```curl -XPOST http://localhost:3000/distribute -H 'content-type: application/json' -d '{"addresses": ["0xb3ee93759350F99308d8883420EbF2280f15491D", "0x9e71fF77E86eD0ac356f96F6D20060C0b59370C5"]}```