const express = require("express")
let erc20 = require('./lab4_erc20_transfer_async.js')
let distribute = require('./distribute.js')

const app = express()

app.use(express.json())

app.get('/symbol', async (req, res) => {
  res.send(await erc20.getSymbol())
})

app.get('/supply', async (req, res) => {
  res.send(await erc20.getTotalSupply())
})

app.get('/balance', async (req, res) => {
  const address = req.query.address
  res.send(await erc20.getBalanceOf(address))
})

app.get('/transfer', async (req, res) => {
  res.send(await erc20.transfer())
})

app.post('/distribute', async(req, res) => {
  const addresses = req.body.addresses
  await distribute.distribute(addresses)
  res.status(200).send('Distribution done!')
})

const port = 3000
app.listen(port, () => console.log(`listening on port ${port}...`))